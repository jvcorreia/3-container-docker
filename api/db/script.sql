CREATE DATABASE IF NOT EXISTS
    escola;
USE escola;

CREATE TABLE IF NOT EXISTS alunos (
    id INT(11) AUTO_INCREMENT,
    name VARCHAR(255),
    matricula INT(11),
    PRIMARY KEY (id)
);

INSERT INTO alunos VALUE(0, 'João Victor', 177);
INSERT INTO alunos VALUE(0, 'Marcos Paulo', 178);